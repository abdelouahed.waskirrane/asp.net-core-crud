﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication2.Models;
using WebApplication2.Services;

namespace WebApplication2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoItemsController : ControllerBase
    {
        private readonly ITodoService _todoItemService;

        public TodoItemsController(ITodoService todoItemService)
        {
            _todoItemService = todoItemService;
        }


        // GET: api/TodoItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TodoItem>> GetTodoItem(int id)
        {
            var todoItem = await _todoItemService.getById(id);

            if (todoItem == null)
            {
                return NotFound($"TodoItem with {id} is not found ");
            }

            return todoItem;
        }

        [HttpGet]
        public async Task<ActionResult<List<TodoItem>>> getAll()
        {
            return await _todoItemService.getAll();

        }   
        [HttpPost]
        public async Task<ActionResult<TodoItem>> postItem(TodoItem todoItem)
        {
            await _todoItemService.add(todoItem);
            return CreatedAtAction("GetTodoItem", new { id = todoItem.Id },todoItem);
        } 
        [HttpPut("{id}")]
        public async Task<ActionResult<TodoItem>> updateItem(int id,TodoItem todoItem)
        {
            var entity = await _todoItemService.Update(id,todoItem);
            if (entity == null)
            {
                return BadRequest($"the todoItem with the ID: {id}");
            }
            return entity;
            
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<TodoItem>> DeleteItem(int id)
        {
            var entity = await _todoItemService.Delete(id);
            if (entity == null)
            {
                return BadRequest($"TodoItem with {id} is not Found");
            }
            return entity;
        }
    }

}
