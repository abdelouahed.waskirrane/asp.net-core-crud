﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Models;
using WebApplication2.Repositories;

namespace WebApplication2.Services
{
    public interface ITodoService
    {
            Task<TodoItem> getById(int id);
            Task<List<TodoItem>> getAll();
            Task<TodoItem> add(TodoItem todoItem);
            Task<TodoItem> Update(int id, TodoItem todoItem);
            Task<TodoItem> Delete(int id);
    }
    public class TodoService : TodoItemRepository,ITodoService
        { 
        private ITodoItemRepository _todoItem;
        public TodoService(RepositoryContext repositoryContext, ITodoItemRepository todoItemRepository):base(repositoryContext)
        {
            
            this._todoItem = todoItemRepository;
        }

       
        public async Task<TodoItem> getById(int id)
        {
            return await this._todoItem.FindByCondition(te => te.Id==id);
        }
        public async Task<List<TodoItem>> getAll()
        {
            return await _todoItem.FindAll();
        } 
        public async Task<TodoItem> add(TodoItem todoItem)
        {
            var  todoItemEntity = await _todoItem.Create(todoItem);
            await this.RepositoryContext.SaveChangesAsync();
            return todoItemEntity;
        }
        public async Task<TodoItem> Update(int id,TodoItem todoItem)
        {
            
            var entity = await _todoItem.FindByCondition(td => td.Id == id);
            if (entity == null)
            {
                return entity;
            }
            todoItem.Id = entity.Id;

            var todoItemEntity = _todoItem.Update(todoItem);
            await this.RepositoryContext.SaveChangesAsync();
            return todoItemEntity;
        }

        public async Task<TodoItem> Delete(int id)
        {
            var entity = await _todoItem.FindByCondition(td => td.Id == id);
            if (entity == null)
            {
                return entity;
            }

            var todoItemEntity = _todoItem.Delete(entity);
            await this.RepositoryContext.SaveChangesAsync();
            return todoItemEntity;
        }
    }
}
