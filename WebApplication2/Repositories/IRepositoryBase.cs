﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebApplication2.Repositories
{
    public interface IRepositoryBase<T>
    {
        Task<List<T>> FindAll();
        Task<T> FindByCondition(Expression<Func<T, bool>> expression);
        Task<T> Create(T entity);
        T Update(T entity);
        T Delete(T entity);
    }

    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected RepositoryContext RepositoryContext { get; set; }
        public RepositoryBase(RepositoryContext repositoryContext)
        {
            this.RepositoryContext = repositoryContext;
        }
        public async Task<List<T>> FindAll()
        {
            return await this.RepositoryContext.Set<T>().AsNoTracking().ToListAsync();
        }
        public async Task<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return await this.RepositoryContext.Set<T>().Where(expression).AsNoTracking().SingleOrDefaultAsync();
        }
        public async Task<T> Create(T entity)
        {
            await this.RepositoryContext.Set<T>().AddAsync(entity);
            return entity;
        }
        public T Update(T entity)
        {
            this.RepositoryContext.Set<T>().Update(entity).State=EntityState.Modified;
            return entity;
        }
        public T Delete(T entity)
        {
            this.RepositoryContext.Set<T>().Remove(entity);
            return entity;
        }
    }
}
