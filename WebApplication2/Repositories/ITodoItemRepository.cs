﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebApplication2.Repositories
{
    public interface ITodoItemRepository : IRepositoryBase<TodoItem>
    {
    }

    public class TodoItemRepository : RepositoryBase<TodoItem>, ITodoItemRepository
    {
        public TodoItemRepository(RepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
